import java.io.*;
import java.net.*;

public class Client {
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private DataOutputStream os;

    private String sHostname;
    //private int iPort;

    public void establishConnection(String hostname, int port) {
        try {
            clientSocket = new Socket(hostname, port);

            out = new PrintWriter(clientSocket.getOutputStream(), true );
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            os = new DataOutputStream(clientSocket.getOutputStream());
        } catch (UnknownHostException e) {
            System.err.println("establishConnection -> Don't know about host: " + hostname);
        } catch (IOException e) {
            System.err.println("establishConnection -> Couldn't get I/O for the connection to: " + hostname);
        }

        sHostname = hostname;
        //iPort = port;
    }
    public void terminateConnection() {
        try {
            in.close();
            out.close();
            clientSocket.close();
        } catch (IOException e) {
            System.err.println("terminateConnection -> Couldn't get I/O for the connection to: " + sHostname);
        }
    }


    public void sendDataPacketCS(Object[] arg) { // Sending data packet from CLIENT to SERVER
        try {
            os.writeByte(arg.length); //send to SERVER how many arguments are passed

            for (Object var : arg) {
                // determine the type of the argument
                if (var instanceof Byte) {
                    os.writeByte((Byte) var);
                } else if (var instanceof String) { //send String to SERVER
                    os.writeUTF((String) var);
                } else if (var instanceof Float) { //send Float to SERVER
                    os.writeFloat((Float) var);
                } else if (var instanceof Double) { //send Double to SERVER
                    os.writeDouble((Double) var);
                } else if (var instanceof Integer) { //send Integer to SERVER
                    os.writeInt((Integer) var);
                } else {
                    System.err.println("sendPacketCS -> Unsupported data type");
                    return;
                }
            }
            os.flush(); // Send off the data
        }
         catch (IOException e) {
            System.out.println("sendDataPacketCS -> IOException");
        }
    }

    public void receiveDataPacketSC() { // Receiving data packet from SERVER to CLIENT
        return;
    }





   public static void main(String[] args) {
        Client client = new Client();
        client.establishConnection("127.0.0.1", 10000);
        client.sendDataPacketCS(args);

        System.out.println("Successfully sended data packet");
        client.terminateConnection();
    }
}
