import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;

public class ClientNIO {
    public static void main(String[] args) throws IOException, InterruptedException {
        for (int i=0; i<10; i++) {
            System.out.println("loop: " + i);
            InetSocketAddress addr = new InetSocketAddress("127.0.0.1", 10000);
            SocketChannel sc = SocketChannel.open(addr);

            System.out.printf("Connecting to Server on port 10000...");

            ArrayList<String> companyDetails = new ArrayList<String>();

            // create a ArrayList with companyName list
            companyDetails.add("Java");
            companyDetails.add("IntelliJ");
            companyDetails.add("Gitlab");
            companyDetails.add("Stackoverflows");
            companyDetails.add("exit");

            for (String companyName : companyDetails) {

                byte[] message = new String(companyName).getBytes();
                ByteBuffer buffer = ByteBuffer.wrap(message);


                PacketLoginRequest packet_login_request = new PacketLoginRequest();
                packet_login_request.bHeader = HeaderCS.HEADER_CS_HASH_PASSWORD;
                packet_login_request.login = companyName;
                sc.write(serializeObject(packet_login_request));

                //          sc.write(buffer);

                System.out.printf("sending: " + companyName);
                buffer.clear();

                // wait for 2 seconds before sending next message
            }

            byte[] exit = new String("exit").getBytes();
            ByteBuffer bfr = ByteBuffer.wrap(exit);
            sc.write(bfr);

            sc.close();
            Thread.sleep(1);
        }
    }

    // Method to convert object to a byte array
    public static ByteBuffer serializeObject(Object obj) throws IOException {
        System.out.println();

        ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bytesOut);
        oos.writeObject(obj);
        oos.flush();
        byte[] bytes = bytesOut.toByteArray();
        bytesOut.close();
        oos.close();

        ByteBuffer bb = ByteBuffer.allocate(256);
        bb.clear();
        bb.put(bytes);
        bb.flip();

        return bb;
    }
}


//ServerSocketChannel ssChannel = ServerSocketChannel.open();
//ssChannel.configureBlocking(true);
//ssChannel.socket().bind(new InetSocketAddress("127.0.0.1", 10000));

//        while (!isConnected) {
//            for (int i=0; i<1; i++) {
//
//                ssChannel = ServerSocketChannel.open();
//                ssChannel.configureBlocking(true);
//                ssChannel.socket().bind(new InetSocketAddress("127.0.0.1", 10000));
//
//                SocketChannel sChannel = SocketChannel.open();
//                sChannel.configureBlocking(false);
//
//                if (sChannel.connect(new InetSocketAddress("127.0.0.1", 10000))) {
//                    ObjectOutputStream objectOS = new ObjectOutputStream(sChannel.socket().getOutputStream());
//
//                    PacketLoginRequest packet_login_request = new PacketLoginRequest();
//                    packet_login_request.bHeader = HeaderCS.HEADER_CS_HASH_PASSWORD;
//                    packet_login_request.login = "ADMIN_" + 1;
//
//
//                    objectOS.writeObject(packet_login_request);
//                    objectOS.close();
//
//                    System.out.println("Connected");
//
//                }

                /*
                PacketTest packet_large = new PacketTest();
                packet_large.bHeader = HeaderCS.HEADER_CS_LARGE;
                packet_large.test1 = "test1";
                packet_large.test2 = "test2";
                packet_large.test3 = 3;
                packet_large.test4 = 4;
                packet_large.test5 = 5.0;
                packet_large.test6 = 6.0;
                packet_large.test7 = 7.0f;
                packet_large.test8 = 8.0f;
                packet_large.process_time = System.currentTimeMillis();
                outputStream.writeObject(packet_large);
                */
//outputStream.close();
//socketchannel.close();

