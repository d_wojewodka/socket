import java.io.Serializable;
import java.util.SortedSet;

class HeaderCS { // CLIENT -> SERVER
    public static byte HEADER_CS_LOGIN_REQUEST = 1;
    public static byte HEADER_CS_HASH_PASSWORD = 2;
    public static byte HEADER_CS_LARGE = 127;
}

class HeaderSC { // SERVER -> CLIENT
    public static byte HEADER_SC_LOGIN_REQUEST = 1;
    public static byte HEADER_SC_HASH_PASSWORD = 2;
    public static byte HEADER_SC_LARGE = 127;
}

class PacketLoginRequest implements Serializable {
    byte bHeader;
    String login;
}

class PacketHashPassword implements Serializable {
    byte bHeader;
    int password_length;
    SortedSet<Integer> password_char;
}

class PacketTest implements Serializable {
    byte bHeader;
    String test1, test2;
    int test3, test4;
    double test5, test6;
    float test7, test8;
    long process_time;


}

