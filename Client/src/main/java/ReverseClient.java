import java.net.*;
import java.io.*;
import java.util.Scanner;

public class ReverseClient {

    public static void main(String[] args) {
        if (args.length < 2) return;

        String hostname = args[0];
        int port = Integer.parseInt(args[1]);

        try (Socket socket = new Socket(hostname, port)) {

            OutputStream output = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(output, true);

            Console console = System.console();
            String login, mysqlhash, response;

            System.out.println("Successfully connected to server!");

            do {
                InputStream input = socket.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));

                Scanner getInput = new Scanner(System.in);
                System.out.println("login: ");
                login = getInput.nextLine();

                writer.println(login);

                System.out.println("password: ");
                mysqlhash = getInput.nextLine();

                writer.println(mysqlhash);

                response = reader.readLine();
                System.out.printf(response);

            } while (!response.equals("exit"));

            socket.close();

        } catch (UnknownHostException ex) {

            System.out.println("Server not found: " + ex.getMessage());

        } catch (IOException ex) {

            System.out.println("I/O error: " + ex.getMessage());
        }
    }
}