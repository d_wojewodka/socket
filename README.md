Core:
- loads: databases, network, pulse(timer)
- main loop

Network:
- listen for new connection and handle output / input
- keep the table with session list

Pulse:
- main timer

Database:
- perform operations on database such as editing / reading / writing

Log:
- prints output into console ||&& write to file (log.txt, error.txt)

helper:
- instance that contains reference to all classes, so it's easier to call functions

Session:
- contains all the data about current connection
