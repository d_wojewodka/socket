import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;


enum col { // Columns in csv, just name
    INDEX, LOGIN, PASSWORD
}


public class CSV {
    public boolean readColumnName = true; // if enabled script get it's from first line of file (usual)
    public String lineSeparator = "\t"; // which character is used to separate values, in this case \t stands for TAB
    public File file;
    public SortedMap<Integer, ArrayList<String>> table = new TreeMap<Integer,  ArrayList<String>>();

    public CSV(String filename) throws IOException {
        file = new File(filename);
    }

    public CSV(File refFile) throws IOException {
        file = refFile;
    }

    public SortedMap<Integer, ArrayList<String>> getTable() {
        return table;
    }

/*    public SortedMap<Integer, SortedSet<String>> readFile() throws IOException {
        br = Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8);
        String line;
        int index = 0;

        while ((line = br.readLine()) != null) {
            SortedSet<String> value_set = new TreeSet();
            for (String value : line.split(lineSeparator)) {

            table.put(index, values);

            index++;
        }
    }*/

    public boolean isDigit(String str) {
        if (str == null)
            return false;

        for (char c : str.toCharArray())
            if (c < '0' || c > '9')
                return false;

        return true;
    }

    public SortedMap<Integer, ArrayList<String>> asTable() throws IOException {
        table.clear();

        BufferedReader br = Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8);
        String line;
        int index = 0;

        while ((line = br.readLine()) != null) {
            ArrayList<String> value_list = new ArrayList();
            for (String value : line.split(lineSeparator)) {
                if (!isDigit(value) && value.length() > 0 && (value.charAt(0) == '"' && value.charAt(value.length()-1) == '"' || value.charAt(0) == '\'' && value.charAt(value.length()-1) == '\''))
                    value_list.add(value.substring(1, value.length() - 1));
                else
                    value_list.add(value);
            }

            table.put(index, value_list);

            index++;
        }

        br.close();
        return table;
    }


    public SortedMap<String, ArrayList<String>> asTablePrimary(int primaryColumn) throws IOException {
        table.clear();

        SortedMap<String, ArrayList<String>> primaryTable = new TreeMap<String,  ArrayList<String>>();
        BufferedReader br = Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8);
        String line;
        int index = 0;

        while ((line = br.readLine()) != null) {
            String key = "";

            if (index != 0) {

                ArrayList<String> value_list = new ArrayList();
                String[] line_split = line.split(lineSeparator);
                for (int i = 0; i < line_split.length; i++) {
                    if (i != primaryColumn) {
                        if (!isDigit(line_split[i]) && line_split[i].length() > 0 &&
                                (line_split[i].charAt(0) == '"' && line_split[i].charAt(line_split[i].length() - 1) == '"' ||
                                        line_split[i].charAt(0) == '\'' && line_split[i].charAt(line_split[i].length() - 1) == '\''))
                            value_list.add(line_split[i].substring(1, line_split[i].length() - 1));
                        else
                            value_list.add(line_split[i]);
                    } else {
                        key = line_split[primaryColumn];
                    }
                }
                primaryTable.put(key, value_list);

                index++;
            }
            else index++;
        }

        br.close();
        return primaryTable;
    }


    public ArrayList<Database.Account> asAccount() throws IOException {
        BufferedReader br = Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8);
        String line;
        int index = 0;

        ArrayList<Database.Account> var = new ArrayList();

        while ((line = br.readLine()) != null) {
            if (index != 0) {
                String[] str = line.split(lineSeparator);

                Database.Account row = new Database.Account();
                row.index = Integer.parseInt(str[0]);
                row.login = str[1];
                row.password = str[2];

                var.add(row);

                index++;
            }
            else index++;
        }

        br.close();
        return var;
    }




    // NOTE
    public void usageAPI() {

        // connect to api

        // key.get(column_id);   starting from 0
        int row = 1;

        for (ArrayList<String> key : table.values()) {
            if (row == 1) { // 1st row is usual filled with column name, so we can skip it
                //key.get(column_id);
                row++;
                continue;
            }

            System.out.println(key);

            // api query for product

            row++;
        }

        // api execute query
    }
}
