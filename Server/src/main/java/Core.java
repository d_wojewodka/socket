import java.io.IOException;
import java.net.URISyntaxException;


public class Core extends Thread {
    public final String ip_address = "127.0.0.1";
    public final int port_address = 10000;

    public boolean shutdown = false;
    public int tick_per_second;
    public long tick_rate; // tics per second
    public long core_pulse_message_time = 60; // time in seconds
    public long tics = 0;

    public TimeStruct last_time;
    public TimeStruct before_sleep;
    public TimeStruct operation_time = new TimeStruct(false);
    public long wake_up_time;


    public void setTickPerSecondRate(int tps) {
        this.tick_per_second = tps;
    }

    public void run() {
        try {
            launch(this.tick_per_second);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean launch(int tps) throws URISyntaxException, IOException, InterruptedException { // tick per second
        // class reference cache
        helper.core = this;
        helper.db = new Database();
        helper.network = new Network(ip_address, port_address);
        helper.pulse = new Pulse(1000000 / tps);

        Thread t;

        // loading databases by single Thread
        LoadTable lt = new LoadTable();
        t = new Thread(lt);
        t.start();
        t.join();

        // starts timer
        t = new Thread(helper.pulse);
        t.start();
        Log.append(LogType.LOG, "[CORE] Pulse Started - tick rate: " + tps + " tick per second");

        // set network online
        helper.network.setOnline();


        // main loop, while timer is running core is working
        while (coreLoop() == 1) {
        }


        destroy();
        Log.append(LogType.LOG, "[CORE] Server is shutting down");
        return true;
    }

    public void destroy() {
        //helper.packetHandle.block(); // TODO cannot accept any new packet
        helper.packetHandle = null;

        helper.db.flushTable(); // saves databases before shutdown
        helper.db = null;

        helper.network.setOffline(); // network is going offline
        helper.network = null;

        helper.pulse.quit(); // stopping server timer
        helper.pulse = null;

        helper.core = null;
    }

    public void coreShutdown() {
        shutdown = true;
    }

    public int coreLoop() {
        int passed_pulses;

        if ((passed_pulses = coreIdle()) == 0) {
            return 0;
        }

        assert(passed_pulses > 0);

        return 1;
    }

    public int coreIdle() {
        coreTick();

        if (shutdown)
            return 0;

        int pulses;

        if ((pulses = helper.pulse.pulseIdle()) == 0)
            return 0;

        return pulses;
    }

    public float coreTickRate() {
        return (float) this.tick_rate;
    }

    public boolean coreIsShutdowned() {
        return this.shutdown;
    }

    public void coreTick() {
        this.tics++;
    }
}
