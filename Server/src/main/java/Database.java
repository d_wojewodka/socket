import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;


public class Database implements Runnable{
    public String[] tables = new String[]{"account", "test"};
    public SortedMap<Integer, ArrayList<String>> account = new TreeMap<Integer, ArrayList<String>>();
    public SortedMap<Integer, ArrayList<String>> test = new TreeMap<Integer, ArrayList<String>>();

    public boolean loaded = false;

    //USAGE:

    //READ VALUES
    //        int idx = 0;
    //        for (ArrayList<String> value : db.account.values()) {
    //            if (idx == 2) {
    //                value.set(2, "null");
    //            }
    //            idx++;
    //        }

    //PRINT TABLE
    //        System.out.println(db.account);
    //        System.out.println(db.test);
    //        db.acc.forEach(Database.Account::print);

    //PRINT ROW
    //----------------------------------------------------------------------------------------------------------------------
    //        for (ArrayList<String> value : db.test.values()) {
    //            int columns = value.size();
    //            System.out.println(value);
    //        }

    //SAVE TABLE
    //       db.saveTable("account",db.account);
    //       db.accountSave();


    public void run() {
        for (String table : tables)
        {
            long start_time = System.currentTimeMillis();
            try {
                if (fillTable(table))
                    Log.append(LogType.LOG, "[DATABASE] successfully loaded table: " + table + " [" + (System.currentTimeMillis()-start_time) + "ms]");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.append(LogType.LOG, "[DATABASE] tables loaded " + tables.length + " total");
    }

    public void flushTable() {
       if (saveTable("account", account) && saveTable("test", test)) {
           Log.append(LogType.LOG, "[DATABASE] tables successfuly saved");
           return;
       }

        Log.append(LogType.ERROR, "[DATABASE] error occured during save databases");
    }

    public boolean fillTable(String table) throws IOException {
        File file = new File(table + ".csv");
        CSV csv = new CSV(file);

        switch (table) {
            case "test": {
                test = csv.asTable();
                return true;
            }
            case "account": {
                account = csv.asTable();
                //accPrimary = csv.asTablePrimary(1);
                //acc = csv.asAccount();
                return true;
            }
            default: {
                Log.append(LogType.ERROR, "Unknown table: " + table);
                return false;
            }
        }
    }

    public boolean accountSave() {
        long process_time = System.currentTimeMillis();
        String content = "";

        int index = 0;
        for (ArrayList<String> row : account.values()) {
            if (index > 0)
                content += System.lineSeparator();

            int column = 0;
            for (String val : row) {
                if (column == row.size()-1)
                    content += val;
                else
                    content += val + "\t";

                column++;
            }
            index++;
        }

        if (content == "")
            return false;

        try {
            File file = new File("account.csv");
            if (!file.exists())
                file.createNewFile();

            Files.write(file.toPath(), content.getBytes());

            Log.append(LogType.LOG, "[DATABASE] successfully saved account.csv [" + (System.currentTimeMillis()-process_time) + "ms]");
            for (int i=0; i<10; i++)
                Log.append(LogType.ERROR, "TEST_" + i);
        } catch (Exception e) {
            Log.append(LogType.ERROR, "[DATABASE] Cannot save database into file");
            e.printStackTrace();
        }

        return true;
    }


    public boolean saveTable(String name, SortedMap<Integer, ArrayList<String>> table) {
        long process_time = System.currentTimeMillis();
        String table_parsed = "";

        int index = 0;
        for (ArrayList<String> row : table.values()) {
            if (index > 0)
                table_parsed += System.lineSeparator();

            int column = 0;
            for (String val : row) {
                if (column == row.size()-1)
                    table_parsed += val;
                else
                    table_parsed += val + "\t";

                column++;
            }

            index++;
        }

        try {
            File file = new File(name + ".csv");


            //String data = "This is a demo of the flush method";
            //FileWriter file2 = new FileWriter("D:\\Java\\Socket\\Server\\target\\classes\\new_table.csv");

            // Creates a BufferedWriter
            //BufferedWriter output2 = new BufferedWriter(file2);

            // Writes data to the file
            //output2.write(data);

            //output2.close();


            Log.append(LogType.LOG, "[DATABASE] successfully saved " + name + " into file " + name + ".csv [" + (System.currentTimeMillis()-process_time) + "ms]");
        } catch (Exception e) {
            Log.append(LogType.ERROR, "[DATABASE] Cannot save database into file");
            e.printStackTrace();
        }

        return true;
    }

    public boolean authenticate(String login, String hash_password) {
        if (account.containsValue(login)) {
            //String password = // TODO decrypt password
            //if (account.get(login)[Account.col.hash_password.ordinal()] == password)
            return true;
        }
        else return false;
    }

    static class Account {
        enum col {
            index,
            login,
            hash_password,
        }

        public int index;
        public String login, password;

        public void print() {
            System.out.println(String.format("index: %d, login: %s, password: %s", index, login, password));
        }
    }

    static class JDBC {} // TODO fetch the data from MySQL via JDBC
}

class LoadTable extends Thread {
    public void run() {
        for (String table : helper.db.tables) {
            long start_time = System.currentTimeMillis();
            try {
                if (helper.db.fillTable(table))
                    Log.append(LogType.LOG, "[DATABASE] successfully loaded table: " + table + " [" + (System.currentTimeMillis() - start_time) + "ms]");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.append(LogType.LOG, "[DATABASE] tables loaded " + helper.db.tables.length + " total");
    }
}
