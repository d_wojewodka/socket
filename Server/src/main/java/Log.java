import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;


enum LogType {
    LOG,
    ERROR
}


public class Log {
    public static boolean console_output = true;
    public static boolean file_output = true;
    public static final String log_filename = "log.txt";
    public static final String error_filename = "error.txt";
    public static File logFile;
    public static File errorFile;


    public static boolean init(LogType log, String message) {
        try {
            if (log == LogType.LOG) {
                if (logFile == null)
                    logFile = new File(log_filename);

                if (!logFile.exists())
                    logFile.createNewFile();

                return true;
            }
            else if (log == LogType.ERROR) {
                if (errorFile == null)
                    errorFile = new File(error_filename);

                if (!errorFile.exists())
                    errorFile.createNewFile();

                return true;
            }
            else {
                System.err.println(log + " is Unknown LogType: " + message);
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean write(LogType log, String output) {
        output = String.format("[%s - %s] %s", LocalDate.now(), LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss:SSS")), output);
        File f;

        if (log == LogType.LOG)
            f = logFile;
        else if (log == LogType.ERROR)
            f = errorFile;
        else {
            System.err.println(log + " is Unknown LogType: " + output);
            return false;
        }

        try {
            Files.write(f.toPath(), output.getBytes(), StandardOpenOption.APPEND);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }

    public static void console(LogType log, String output) {
        if (log == LogType.LOG)
            System.out.println(output);
        else if (log == LogType.ERROR)
            System.err.println(output);
        else
            System.err.println(log + " is Unknown LogType: " + output);

        return;
    }

    public static void append(LogType log, String output) {
        if (file_output) {
            if (!init(log, output))
                return;

            write(log, output);
        }
        if (console_output)
            console(log, output);

        return;
    }
}
