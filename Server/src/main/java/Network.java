import java.io.IOException;
import java.net.*;
import java.util.HashMap;

public class Network {
    public String host;
    public int port;
    public ServerSocket ss;
    public Socket s;

    public boolean online = true;

    public HashMap<String, Session> m_sessionList = new HashMap<String, Session>();  // list of current sessions


    public Network(String host, int port) {
        this.host = host;
        this.port = port;
        helper.network = this;
    }


    public void setOnline() throws IOException {
        // bind server to port
        ss = new ServerSocket(port);
        Log.append(LogType.LOG, "[NETWORK] Server started listening " + this.host + ":" + this.port);

        try {
            // infinite loop that accepts and handle session
            while (online) {
                // wait for the client to make connection
                s = ss.accept();
                Log.write(LogType.LOG, "New connection from " + s.getRemoteSocketAddress());

                // create new session
                InetSocketAddress sAddr = (InetSocketAddress) s.getRemoteSocketAddress();
                Session session = new Session(s);

                // put session into sessions list
                m_sessionList.put(sAddr.getHostName(), session);
                // connection is handled by a Thread
                new Thread(session).start();
            }

            // when loop break, network is offline
            s.close();
            ss.close();
            Log.append(LogType.LOG, "[NETWORK] network is going offline");
        } catch (IOException e) {
            // silent exception
        }
    }

    public void setOffline() {
        this.online = false;
    }
}
