// C10k
// https://www.programmersought.com/article/6295991350/

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;


public class Network_v1 implements Runnable {
    private InetAddress host;
    private Selector selector;
    private ServerSocketChannel ssc;
    private int port;

    private volatile boolean stop;

    private int connectionBottleneck = 1024;
    public TreeMap<String, Session> cacheNetwork = new TreeMap<String, Session>();
    public boolean authorized = false; // when clients loggin changes to true


    public Network_v1(String ip, int port){
        try {
            this.host = InetAddress.getByName("127.0.0.1");
            this.port = port;
            this.selector = Selector.open();

            ssc = ServerSocketChannel.open();
            ssc.configureBlocking(false);
            ssc.bind(new InetSocketAddress(host, 10000), connectionBottleneck);
            ssc.register(selector, SelectionKey.OP_ACCEPT);



            SelectionKey key = null;
            Log.append(LogType.LOG, "[NETWORK] started listening: /" + host + ":" + this.port);

            while (true) {
                if (selector.select() <= 0)
                    continue;

                Set<SelectionKey> selectedKeys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = selectedKeys.iterator();
                while (iterator.hasNext()) {
                    key = (SelectionKey) iterator.next();
                    iterator.remove();

                    if (key.isAcceptable()) {
                        SocketChannel sc = ssc.accept();
                        sc.configureBlocking(false);
                        sc.register(selector, SelectionKey.OP_READ);
                        Log.append(LogType.LOG, "[NETWORK] Session " + sc.getRemoteAddress() + " accepted");
                    }
                    if (key.isReadable()) {
                        SocketChannel sc = (SocketChannel) key.channel();
                        ByteBuffer bb = ByteBuffer.allocate(1024);
                        sc.read(bb);

                        String result = new String(bb.array()).trim();
                        System.out.println("[NETWORK] Received Packet: " + result);

                        if (result.length() <= 0) {
                            Log.append(LogType.LOG, "[NETWORK] Session " + sc.getRemoteAddress() + " closed");
                        }
                    }

                }
            }



        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void stop(){
        this.stop = true;
    }

    public String getIpAddress(SocketAddress remote) {
        return remote.toString().replace("/", "").split(":")[0];
    }

    public void run() {
        long pulse_count = helper.core.tics;

        while(!stop){
            try {
                // synchronized with pulse
                Thread.sleep(helper.core.wake_up_time > System.currentTimeMillis() ?
                        (helper.core.wake_up_time - System.currentTimeMillis()) : 0);

                selector.select(1000);
                //5. Selector polls all SocketChannels registered on it and returns all SocketChannels with events of interest to the server.
                Set<SelectionKey> selectedKeys = selector.selectedKeys();
                Iterator<SelectionKey> it = selectedKeys.iterator();
                SelectionKey key = null;

                //6, the server iterates through all the SocketChannels that need to be processed.
                while (it.hasNext()) {
                    key = it.next();

                    // Remove the unprocessed queue
                    it.remove();
                    try {
                        handleInput(key);
                    } catch (Exception e) {
                        if (key != null) {
                            key.cancel();
                            if (key.channel() != null)
                                key.channel().close();
                        }
                    }
                }
            } catch(IOException | InterruptedException e){
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        // After the multiplexer is turned off, all resources such as Channel and Pipe registered above will be automatically removed.
        //Register and close, so there is no need to close resources repeatedly
        if(selector != null){
            try {
                selector.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void handleInput(SelectionKey key) throws IOException, ClassNotFoundException {
        // Determine whether the key value is valid
        if(key.isValid()){
            //6, Selector listens for new client access, handles session requests for new access
            if(key.isAcceptable()){
                /*
                 * Receive client session request and create SocketChannel instance via accept of ServerSocketChannel
                 * After completing the above operation, it is equivalent to completing the three-way handshake of TCP, and the TCP physical link is officially established.
                 * We set the SocketChannel to asynchronous non-blocking
                 * It is also possible to set its TCP parameters, such as the size of the TCP send and receive buffers, etc.
                 */


                ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
                SocketChannel sc = ssc.accept();

                //String incoming_ip = helper.network.getIpAddress(sc.getRemoteAddress());
                //Session con = new Session(incoming_ip, port);

                // Set the client link to non-blocking mode
                sc.configureBlocking(false);
                //8, register the newly accessed SocketChannel to the Selector, and listen for the read operation.
                sc.register(selector,SelectionKey.OP_READ);

                //if (helper.network.cacheNetwork.containsValue(incoming_ip)) {
                //    helper.network.cacheNetwork.put(incoming_ip, con);
                //    Log.append(LogType.LOG, "Session from: " + incoming_ip + " has been added to Session list");
                //}
//                else {
//                    Log.append(LogType.ERROR, "[CONNECTION] Session with  already exists");
//                    return;
//                }
            }
            //9, listening to the registered SocketChannel has a readable event, processing
            if(key.isReadable()){
                /*
                 * Read the client's request message
                 * We can't know the size of the stream sent by the client. As a routine, we open a 1k buffer.
                 * Then helper the read method to read the request stream
                 */
                // Read the data

                ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
                SocketChannel socketChannel = serverSocketChannel.accept();
                socketChannel.configureBlocking(false);
                SelectionKey readKey = socketChannel.register(selector, SelectionKey.OP_READ);
                readKey.attach(key.attachment());

                System.out.println(key.attachment());
                helper.packetHandle.receivePacket(key.attachment());


//                SocketChannel sc = (SocketChannel) key.channel();
//
//                //SocketChannel test = SocketChannel.open();
//                sc.configureBlocking(false);
//                if (sc.connect(new InetSocketAddress(helper.core.ip_address, helper.core.port_address))) {
//                    System.out.println("connected");
//
//                    ObjectInputStream objectIS = new ObjectInputStream(sc.socket().getInputStream());
//
//                    helper.packetHandle.receivePacket(objectIS.readObject());
//                }
//
//
//                //10, allocate a new cache space, the size is 1024, asynchronously read the client's message
//                ByteBuffer readBuffer = ByteBuffer.allocate(1024);
//                int readBytes = sc.read(readBuffer);
//                /*
//                 * three return values ​​of the read() method
//                 * The return value is greater than 0: read directly, encode and decode the byte
//                 * The return value is equal to 0: no bytes are read, it is a normal scene, ignored
//                 * The return value is -1: the link is closed, you need to close the SocketChannel to release the resource.
//                 */
//                if(readBytes > 0){
//                    ObjectInputStream is = new ObjectInputStream(sc.socket().getInputStream());
//                    helper.packetHandle.receivePacket(is.readObject());
//
//
//                    readBuffer.flip();
//                    // Open up a space, the size is the number of bytes remaining in the buffer
//                    byte[] bytes = new byte[readBuffer.remaining()];
//                    readBuffer.get(bytes);
//                    String body = new String(bytes,"UTF-8");
//                    Log.append(LogType.LOG, "The time server receive order : " + body);
//                    String currentTime = "QUERY TIME ORDER".equalsIgnoreCase(body) ? new java.util.Date(
//                            System.currentTimeMillis()).toString() : "BAD ORDER";
//
//
//                    // packet handle
//                }else if(readBytes < 0){
//                    //The peer link is closed.
//                    key.cancel();
//                    sc.close();
//                }else
//                    ;//Read to 0 byte ignore
            }
        }
    }

    private void doWrite(SocketChannel channel, String response) throws IOException {
        // If the received message is not empty, and is not a blank line
        // The strim method can be used to trim whitespace from the beginning and end of a string (as defined above).
        if(response != null && response.trim().length() > 0){
            byte[] bytes = response.getBytes();
            //9, send the message asynchronously to the client
            // allocate the write space buffer
            ByteBuffer writeBuffer = ByteBuffer.allocate(bytes.length);
            // Put the bytes into the write cache
            writeBuffer.put(bytes);
            /*
             * Flip this buffer and set the limit to the current position.
             * When using the put() method, position is at the end of the data, we need to move it to 0
             * When we helper the get operation, we can copy the byte array in the buffer to the newly created direct array.
             */
            writeBuffer.flip();
            // Call the write method to send the byte array in the buffer area
            // Need to deal with the scene of writing half a package
            channel.write(writeBuffer);
        }
    }
}