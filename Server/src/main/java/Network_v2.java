import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Network_v2 implements Runnable {
    private Selector selector;
    private ServerSocketChannel ssc;
    private InetSocketAddress addr;
    private SelectionKey skey;

    public void init() throws IOException {
        // Selector: multiplexor of SelectableChannel objects
        selector = Selector.open(); // selector is open here

        // ServerSocketChannel: selectable channel for stream-oriented listening sockets
        ssc = ServerSocketChannel.open();
        addr = new InetSocketAddress("127.0.0.1", 10000);

        // Binds the channel's socket to a local address and configures the socket to listen for connections
        ssc.bind(addr);

        // Adjusts this channel's blocking mode.
        ssc.configureBlocking(false);

        int ops = ssc.validOps();
        skey = ssc.register(selector, ops, null);
    }

    @Override
    public void run() {
        Log.append(LogType.LOG, "[NETWORK] Started listening for incoming connections...");

        // Infinite loop..
        // Keep server running
        while (helper.core.coreIdle() == 1) {
            try {
//                if (helper.core.wake_up_time > 0) {
//                    System.out.println("next wake up " + helper.core.wake_up_time + " | " + System.currentTimeMillis() + " | " + (helper.core.wake_up_time - System.currentTimeMillis()) + "ms");
//                    Thread.sleep(helper.core.wake_up_time > System.currentTimeMillis() ?
//                            (helper.core.wake_up_time - System.currentTimeMillis()) : 0);
//                }
                // Selects a set of keys whose corresponding channels are ready for I/O operations
                selector.select();

                // token representing the registration of a SelectableChannel with a Selector
                Set<SelectionKey> keys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = keys.iterator();

                while (iterator.hasNext()) {
                    SelectionKey myKey = iterator.next();

                    // Tests whether this key's channel is ready to accept a new socket session
                    if (myKey.isAcceptable()) {
                        SocketChannel sc = ssc.accept();

                        // Adjusts this channel's blocking mode to false
                        sc.configureBlocking(false);

                        // Operation-set bit for read operations
                        sc.register(selector, SelectionKey.OP_READ);
                        Log.append(LogType.LOG, "[NETWORK] Session Accepted: " + sc.getRemoteAddress());

                        // Tests whether this key's channel is ready for reading
                    } else if (myKey.isReadable()) {
                        SocketChannel sc = (SocketChannel) myKey.channel();
                        ByteBuffer buffer = ByteBuffer.allocate(256);
                        sc.read(buffer);
                        String result = new String(buffer.array()).trim();

                        if (result.length() > 0)
                            Log.append(LogType.LOG, "[NETWORK] Message received: " + result);

                        if (result.equals("exit")) {
                            sc.close();
                            Log.append(LogType.LOG, "[NETWORK] Session droppped");
                        }
                    }
                    iterator.remove();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
