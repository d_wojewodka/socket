import java.io.Serializable;
import java.util.SortedSet;

class HeaderCS { // CLIENT -> SERVER
    public static byte HEADER_CS_LOGIN_REQUEST = 1;
    public static byte HEADER_CS_HASH_PASSWORD = 2;
    public static byte HEADER_CS_LARGE = 127;
}

class HeaderSC { // SERVER -> CLIENT
    public static byte HEADER_SC_LOGIN_REQUEST = 1;
    public static byte HEADER_SC_HASH_PASSWORD = 2;
    public static byte HEADER_SC_LARGE = 127;
}

class PacketLoginRequest implements Serializable {
    byte bHeader;
    String login;

    @Override
    public String toString() { return String.format("PacketLoginRequest: bHeader: %s, login: %s", bHeader, login); }

}

class PacketHashPassword implements Serializable {
    byte bHeader;
    int password_length;
    SortedSet<Integer> password_char; // returns to client character index which is needed to be entered
                        // eg password (test123) -> hash = (*est*2*)
                        // characters "*" must be correct to successfully login
                        // hash password function is already written in my previous project "JavaFX"

    @Override
    public String toString() { return String.format("PacketHashPassword: bHeader: %s, password_length: %s, password_char: %s", bHeader, password_length, password_char); }
}

class PacketTest implements Serializable { // just for test purposes to check the transfer time
    byte bHeader;
    String test1, test2;
    int test3, test4;
    double test5, test6;
    float test7, test8;
    long process_time;

    public String toString() { return String.format("PacketTest: bHeader: %s, test1: %s, test2: %s, test3: %d, test4: %d, test5: %d, test6: %d, test7: %.f, test8: %.f, process_time: %dms", test1, test2, test3, test4, test5, test6, test7, test8, process_time); }
}


