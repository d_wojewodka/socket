import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class PacketHandle {
    public boolean validate(String packet, byte received, byte header) {
        if (received != header) {
            Log.append(LogType.ERROR, "[%s] Packet mismatch - received: [%d], required: [%d]");
            return false;
        }

        return true;
    }

    public boolean compareHeaders(byte header1, byte header2) {
        if (header1 != header2) {
            Log.append(LogType.ERROR, ">> Header mismatch - error");
            return false;
        }

        return true;
    }

    public int receivePacket(Object obj) {
        String packetname = obj.getClass().getCanonicalName();

        switch (packetname) {
            case "PacketTest": {
                PacketTest packet = (PacketTest) obj;

                if (!compareHeaders(packet.bHeader, HeaderCS.HEADER_CS_LARGE)) {
                    return 0;
                }

                System.out.println("Object received: " + packet.getClass().getCanonicalName());
                System.out.println("test1: " + packet.test1);
                System.out.println("test2: " + packet.test2);
                System.out.println("test3: " + packet.test3);
                System.out.println("test4: " + packet.test4);
                System.out.println("test5: " + packet.test5);
                System.out.println("test6: " + packet.test6);
                System.out.println("test7: " + packet.test7);
                System.out.println("test8: " + packet.test8);
                System.out.println("Process time: " + (System.currentTimeMillis() - packet.process_time) + "ms");
                return 1;
            }
            case "PacketLoginRequest": {
                PacketLoginRequest packet = (PacketLoginRequest) obj;

                if (!compareHeaders(packet.bHeader, HeaderCS.HEADER_CS_LOGIN_REQUEST)) {
                    return 0;
                }

                System.out.println(packet.toString());

                System.out.println("Given login: " + packet.login);
            }
            case "PacketHashPassword":
                return 1;
            default:
                Log.append(LogType.ERROR, ">> Unknown packet - " + obj.getClass().getCanonicalName());
                return 0;
        }
    }







    public static void handlePacket(ByteBuffer bb) {
        String packet  = StandardCharsets.UTF_8.decode(bb).toString();
        String[] arr = packet.split(":");
        Byte header = Byte.valueOf(arr[0]);


        try {
            switch (header) {
                case 1: {
                    System.out.println("TestPacket\n\targuments: " + Arrays.toString(arr[1].split(",")));
                    //TestPacket(arr[1]) // arguments
                    break;
                }
                case 2: {
                    System.out.println("LoginPacket\n\targuments: " + Arrays.toString(arr[1].split(",")));
                    //LoginPacket(arr[1]) // arguments
                    break;
                }
                default: {
                    System.err.println("Unknown packet: " + arr[0] + "\n\targuments: " + arr[1]);
                    break;
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Packet: " + packet + " has no arguments, propably it's a message");
        }

        return;
    }
}
