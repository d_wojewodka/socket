public class Pulse extends Core implements Runnable {
    public boolean running = false;

    Pulse(int tps) {
        this.running = true;
        helper.core.operation_time.sec = 0;
        helper.core.operation_time.usec = tps;
        helper.core.tick_rate = 1000000 / tps;
        helper.core.last_time = new TimeStruct();
        helper.core.before_sleep = new TimeStruct();
    }

    public void run() {
        pulseIdle();
    }

    public int pulseIdle() {
        while (running) {

            helper.core.wake_up_time = 0;

            TimeStruct process_time, timeout, temp_time;
            int missed_pulse;

            helper.core.before_sleep = new TimeStruct();
            process_time = TimeStruct.timeDiff(helper.core.before_sleep, helper.core.last_time);

            if (process_time.sec == 0 && process_time.usec < helper.core.operation_time.usec)
                missed_pulse = 0;
            else {
                missed_pulse = (int) (process_time.sec * helper.core.tick_rate);
                missed_pulse += process_time.usec / helper.core.operation_time.usec;
            }

            if (missed_pulse > 0)
                helper.core.last_time = new TimeStruct();
            else {
                temp_time = TimeStruct.timeDiff(helper.core.operation_time, process_time);
                helper.core.last_time = TimeStruct.timeAdd(helper.core.before_sleep, temp_time);

                timeout = TimeStruct.timeDiff(helper.core.last_time, new TimeStruct());

                try {
                    if (helper.core.core_pulse_message_time != 0) {
                        long print_time = (helper.core.core_pulse_message_time * 1000) / (1000 / helper.core.tick_rate);
                        if (helper.core.tics >= print_time && helper.core.tics % print_time == 0)
                            Log.append(LogType.LOG, String.format("[PULSE] %d seconds passed, core has refreshed %d times", helper.core.core_pulse_message_time, helper.core.tics));
                    }

                    long sleep_time = timeout.sec * 1000 + timeout.usec / 1000;
                    helper.core.wake_up_time = System.currentTimeMillis() + sleep_time;
                    Thread.sleep(sleep_time);
                } catch (InterruptedException e) {
                    Log.append(LogType.ERROR, "pulse_idle -> InterruptedException");
                }
            }

            missed_pulse++;

            if (missed_pulse <= 0) {
                Log.append(LogType.ERROR, String.format("[PULSE] missed_pulse is not positive! (%d)", missed_pulse));
                missed_pulse = 1;
            }

            if (missed_pulse > (30 * helper.core.tick_rate)) {
                Log.append(LogType.LOG, String.format("[PULSE] Losing %d seconds. (lag occured)", missed_pulse / helper.core.tick_rate));
                missed_pulse = (int) (30 * helper.core.tick_rate);
            }
            return missed_pulse;
        }

        Log.append(LogType.LOG, "[PULSE] pulse has been stopped");
        return 0;
    }

    public long synchronizePulse() {
        return (helper.core.wake_up_time - System.currentTimeMillis()) > 0 ? (helper.core.wake_up_time - System.currentTimeMillis()) : 1000 / helper.core.tick_per_second;
    }

    public void quit() {
        this.running = false;
    }
}

class TimeStruct {
    public long sec;
    public long usec;

    TimeStruct() {
        long millis = System.currentTimeMillis();
        sec = (millis / 1000);
        usec = (millis % 1000) * 1000;
    }

    TimeStruct(long a, long b) {
        sec = a;
        usec = b;
    }

    TimeStruct(boolean null_time) {
        if (null_time) {
            sec = 0;
            usec = 0;
        }
    }

    public static TimeStruct timeDiff(TimeStruct a, TimeStruct b) {
        TimeStruct rslt = new TimeStruct(false);

        if (a.sec < b.sec)
            return new TimeStruct(true);
        else if (a.sec == b.sec) {
            if (a.usec < b.usec)
                return new TimeStruct(true);
            else {
                rslt.sec = 0;
                rslt.usec = a.usec - b.usec;
                return rslt;
            }
        }
        else {
            rslt.sec = a.sec - b.sec;

            if (a.usec < b.usec) {
                rslt.usec = a.usec + 1000000 - b.usec;
                rslt.sec--;
            }
            else
                rslt.usec = a.usec - b.usec;

            return rslt;
        }
    }

    public static TimeStruct timeAdd(TimeStruct a, TimeStruct b)
    {
        TimeStruct rslt = new TimeStruct(false);

        rslt.sec = a.sec + b.sec;
        rslt.usec = a.usec + b.usec;

        while (rslt.usec >= 1000000) {
            rslt.usec -= 1000000;
            rslt.sec++;
        }

        return rslt;
    }
}
