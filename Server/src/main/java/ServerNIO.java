import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class ServerNIO {
    private int port = 10000;
    private String ip = "127.0.0.1";
    private SocketChannel socketChannel;
    private SelectionKey selkey;
    private ByteBuffer buf;

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ServerNIO server = new ServerNIO();
        server.receiveObject();
    }

    ServerNIO(SelectionKey selkey, SocketChannel chan) throws Throwable {
        this.selkey = selkey;
        this.socketChannel = chan;
    }
    ServerNIO() { }

    void receiveObject() throws IOException, ClassNotFoundException, SocketException {
        socketChannel = createSocketChannel();
        ObjectInputStream is = new ObjectInputStream(socketChannel.socket().getInputStream());

        while (socketChannel.isConnected()) {
            handlePacket(is.readObject());
        }
    }

    private SocketChannel createSocketChannel() throws IOException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.socket().bind(new InetSocketAddress(ip, port));
        socketChannel = serverSocketChannel.accept();

        System.out.println("New session from " + socketChannel.getRemoteAddress());
        return socketChannel;
    }

    public int handlePacket(Object obj) {
        System.out.println("TEST");

        switch (obj.getClass().getCanonicalName()) {
            case "PacketLarge": {
                PacketTest packet = (PacketTest) obj;

                if (packet.bHeader != HeaderCS.HEADER_CS_LARGE) {
                    System.err.println(">> Header mismatch - error");
                    return 0;
                }

                System.out.println("Object received: " + packet.getClass().getCanonicalName());
                System.out.println("test1: " + packet.test1);
                System.out.println("test2: " + packet.test2);
                System.out.println("test3: " + packet.test3);
                System.out.println("test4: " + packet.test4);
                System.out.println("test5: " + packet.test5);
                System.out.println("test6: " + packet.test6);
                System.out.println("test7: " + packet.test7);
                System.out.println("test8: " + packet.test8);
                System.out.println("Process time: " + (System.currentTimeMillis() - packet.process_time) + "ms");
                return 1;
            }
            case "PacketLoginRequest": {
                PacketLoginRequest packet = (PacketLoginRequest) obj;

                if (packet.bHeader != HeaderCS.HEADER_CS_LOGIN_REQUEST) {
                    System.err.println(">> Header mismatch - " + packet.bHeader);
                    return 0;
                }

                System.out.println("Given login: " + packet.login);
            }
            case "PacketHashPassword":
                return 1;
            default:
                System.err.println(">> Unknown packet - " + obj.getClass().getCanonicalName());
                return 0;
        }

    }

}
