import java.net.Socket;


public class Session implements Runnable {
    public Socket s;

    public boolean authorized = false;

    public Session(Socket s) {
        this.s = s;
    }

    public boolean authorize(String login, String password) {
        if (helper.db.authenticate(login, password)) { // TODO if login and password match in database
            authorized = true;
            return true;
         }

        // TODO return message to client
        // "unknown username or password"
        return false;
    }

    public void run() {
        // TODO code to receive and handle data
    }
}

