public class helper {
    // This class was made to make easier calling other class
    // it is like standby mode for running class


    // loads and makes operations on data;
    public static Database db;

    // it is some kind of timer
    public static Pulse pulse;

    // core
    public static Core core;

    // make and listen to connections
    public static Network network;

    // handling data packets
    public static PacketHandle packetHandle;


}
