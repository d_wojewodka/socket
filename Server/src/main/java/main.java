import java.io.IOException;
import java.net.URISyntaxException;

public class main {
    public static void main(String[] args) throws IOException, URISyntaxException, InterruptedException {
        helper.core = new Core();
        helper.core.setTickPerSecondRate(25); // how many refresh per second
        //core.setTickPerSecondRate(Integer.parseInt(args[0]));
        //core.launch(25); // how many refresh per second
        new Thread(helper.core).start();
    }
}
