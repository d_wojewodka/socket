import java.util.TreeMap;

public class testConnection {
    public TreeMap<String, testConnection> conn = new TreeMap<String, testConnection>();
    public int conn_id;
    int conn_value;

    testConnection(int id, int value) {
        this.conn_id = id;
        this.conn_value = value;
    }

    public void start() {
        testConnection tc;

        tc = new testConnection(1, 111);
        this.conn.put("1", tc);

        tc = new testConnection(2, 222);
        this.conn.put("2", tc);

        tc = new testConnection(3, 333);
        this.conn.put("3", tc);

        System.out.println(conn);

        System.out.println(conn.get("3").conn_id);
        System.out.println(conn.get("3").conn_value);
        conn.get("3").conn_value = 10451251;
        conn.get("3").conn_id = 00000000;
        System.out.println(conn.get("3").conn_id);
        System.out.println(conn.get("3").conn_value);

        getConnnection("7");
        getConnnection("8");
        getConnnection("2");

    }


    public testConnection getConnnection(String ip) {
        if (!conn.containsKey(ip) && !conn.containsValue(this)) {
            Log.append(LogType.LOG, "[NETWORK] There is no such a session: " + ip);
            return this;
        }

        Log.append(LogType.LOG, "[NETWORK] found session in cache: " + ip);
        return conn.get(ip);
    }

    public static void main(String... args) {
        testConnection tc = new testConnection(0,0);
        tc.start();
    }
}


