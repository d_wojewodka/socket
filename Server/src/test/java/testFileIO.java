import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

public class testFileIO {
    public File file;

    public testFileIO(String path) throws IOException {
        file = new File(path);
    }

    public void check() throws IOException {
        if (file.exists()) System.out.println("file exists");
        else {
            System.out.println("file does not exists");
            file.createNewFile();
         }
    }


    public static void main(String[] args) throws IOException {
        testFileIO test = new testFileIO("testPacket.csv");
        test.check();
        test.writeToFile("duduaudasuduas", StandardOpenOption.APPEND);
        test.writeToFile("TASRT", StandardOpenOption.APPEND);
        test.writeToFile("14141241142", StandardOpenOption.APPEND);
        test.writeToFile("#####", StandardOpenOption.APPEND);
        test.writeToFile("@@###", StandardOpenOption.APPEND);
        test.writeToFile("%%w#%#%#%#%#%#%#%#", StandardOpenOption.APPEND);
    }

    public boolean writeToFile(String str, StandardOpenOption... oo) throws IOException {
        Files.write(file.toPath(), str.getBytes(), oo);

        return true;
    }
}
