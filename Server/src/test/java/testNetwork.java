// C10k
// https://www.programmersought.com/article/6295991350/

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;


public class testNetwork {
    public static void main(String[] args) throws IOException {
        System.out.println("Sender Start");

        ServerSocketChannel ssChannel = ServerSocketChannel.open();
        ssChannel.configureBlocking(true);
        int port = 12345;
        ssChannel.socket().bind(new InetSocketAddress(port));

        String obj ="testtext";
        while (true) {
            SocketChannel sChannel = ssChannel.accept();

            ObjectOutputStream oos = new
                    ObjectOutputStream(sChannel.socket().getOutputStream());
            oos.writeObject(obj);
            oos.close();

            System.out.println("Session ended");
        }
    }
}


//public class testNetwork {
//    private Selector selector;
//    private ServerSocketChannel servChannel;
//    private volatile boolean stop;
//
//    public static void main(String[] args) throws IOException {
//        int port = 10000;
//        if (args != null && args.length > 0) {
//            try{
//                port = Integer.valueOf(args[0]);
//            }catch(NumberFormatException e){
//
//            }
//        }
//
//        Network_v1 timeServer = new Network_v1("127.0.0.1", port);
//
//        new Thread(timeServer,"NIO-timeServer-001").start();
//    }
//}


