import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class testPacket {
    public static void main(String[] args) throws InterruptedException {
        handlePacket(ByteBuffer.wrap(("TestPacket:10,11,12" + ":" + System.currentTimeMillis()).getBytes()));
        Thread.sleep(10);
        handlePacket(ByteBuffer.wrap("LoginPacket:login,password".getBytes()));
        Thread.sleep(10);
        handlePacket(ByteBuffer.wrap("testPacket:1211421,124".getBytes()));
        Thread.sleep(10);
        handlePacket(ByteBuffer.wrap("so_wrong:1211421,124".getBytes()));
        Thread.sleep(10);
        handlePacket(ByteBuffer.wrap("packet1241251515".getBytes()));
    }

    public static void handlePacket(ByteBuffer bb) {
        String packet  = StandardCharsets.UTF_8.decode(bb).toString();
        String[] arr = packet.split(":");

        if (arr.length == 3)
            System.out.println(System.currentTimeMillis() - Long.valueOf(arr[2]) + "ms");

        try {
            switch (arr[0]) {
                case "TestPacket": {
                    System.out.println("TestPacket\n\targuments: " + Arrays.toString(arr[1].split(",")));
                    //TestPacket(arr[1]) // arguments
                    break;
                }
                case "LoginPacket": {
                    System.out.println("LoginPacket\n\targuments: " + Arrays.toString(arr[1].split(",")));
                    //LoginPacket(arr[1]) // arguments
                    break;
                }
                default: {
                    System.err.println("Unknown packet: " + arr[0] + "\n\targuments: " + arr[1]);
                    break;
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Packet: " + packet + " has no arguments, propably it's a message");
        }

        return;
    }
}
